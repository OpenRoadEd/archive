As the Visual Studio project does not hold Debugging Working Directory information, It is needed to change the following property:

Project properties -> Debugging -> Working Directory

to:

$(SolutionDir)/Debug		For Debug configuration

and

$(SolutionDir)/Release		For Release configuration

